from flask import Flask, request
import mysql.connector
import json
#from flask_cors import CORS

app = Flask(__name__)
#CORS(app)
#CORS(app, origins="http://127.0.0.1:8080", allow_headers=[
 #   "Content-Type", "Authorization", "Access-Control-Allow-Credentials"],
  #  supports_credentials=True)


@app.route("/")
def hello():
    return "Hello World!"



@app.route("/register",methods=['POST'])
def submit_create_new_account():
    data = json.loads(request.data)
    fname = data['fname']
    lname = data['lname']
    contact = data['contact']
    email = data['email']
    username = data['username']
    password = data['password']

    #print("separate",fname,lname,contact,email,username,password)
    print(contact)
    try:
      connection = mysql.connector.connect(host='localhost',database='new1',user='root',password='am')
      query1 = """SET FOREIGN_KEY_CHECKS=0"""
      query = """INSERT INTO users (first_name, last_name, contact_no, email_id, username, password) VALUES (%s, %s, %s, %s, %s, %s) """
      val = (fname, lname, contact, email, username, password)
      query2 = """SET FOREIGN_KEY_CHECKS=1"""
      cursor = connection.cursor()
      res = cursor.execute(query1)
      result = cursor.execute(query, val)
      re = cursor.execute(query2)
      connection.commit()
      print("Record Inserted Successfully")

    except mysql.connector.Error as error :
      connection.rollback() #rollback if any exception occured
      print("Failed inserting record into python_users table {}".format(error))

    finally:
      #closing database connection.
      if(connection.is_connected()):
          cursor.close()
          connection.close()
          print("MySQL connection is closed")
    return 'OK'


@app.route("/lecturer_submit",methods=['POST'])
def submit_lecturer():
    data = json.loads(request.data)
    lid = data['lid']
    fname = data['fname']
    lname = data['lname']
    hquali = data['hquali']
    email = data['email']
    contact = data['contact']
    
    
    #print("separate",fname,lname,contact,email,username,password)
    print(contact)
    try:
      connection = mysql.connector.connect(host='localhost',database='new1',user='root',password='am')
      query1 = """SET FOREIGN_KEY_CHECKS=0"""
      query = """INSERT INTO lecturer (lecturer_id, first_name, last_name, qualification, email_id,contact_no) VALUES (%s, %s, %s, %s, %s, %s) """
      val = (lid, fname, lname, hquali, email, contact)
      query2 = """SET FOREIGN_KEY_CHECKS=1"""
      cursor = connection.cursor()
      res = cursor.execute(query1)
      result = cursor.execute(query, val)
      re = cursor.execute(query2)
      connection.commit()
      print("Record Inserted Successfully")

    except mysql.connector.Error as error :
      connection.rollback() #rollback if any exception occured
      print("Failed inserting record  {}".format(error))

    finally:
      #closing database connection.
      if(connection.is_connected()):
          cursor.close()
          connection.close()
          print("MySQL connection is closed")
    return 'OK'



@app.route("/lecturer_update",methods=['POST'])
def update_lecturer():
    data = json.loads(request.data)
    lid = data['lid']
    fname = data['fname']
    lname = data['lname']
    hquali = data['hquali']
    email = data['email']
    contact = data['contact']
    
    
    #print("separate",fname,lname,contact,email,username,password)
    #print(contact)
    try:
      connection = mysql.connector.connect(host='localhost',database='new1',user='root',password='am')
      query1 = """SET FOREIGN_KEY_CHECKS=0"""
      query = """UPDATE lecturer set first_name = %s, last_name = %s, qualification = %s, email_id = %s,contact_no = %s where lecturer_id = %s """
      val = ( fname, lname, hquali, email, contact, lid)
      query2 = """SET FOREIGN_KEY_CHECKS=1"""
      cursor = connection.cursor()
      res = cursor.execute(query1)
      result = cursor.execute(query, val)
      re = cursor.execute(query2)
      connection.commit()
      print("Record Updated Successfully")

    except mysql.connector.Error as error :
      connection.rollback() #rollback if any exception occured
      print("Failed updateing record  {}".format(error))

    finally:
      #closing database connection.
      if(connection.is_connected()):
          cursor.close()
          connection.close()
          print("MySQL connection is closed")
    return 'OK'



@app.route("/lecturer_delete",methods=['POST'])
def delete_lecturer():
    data = json.loads(request.data)
    lid = data['lid']
    #fname = data['fname']
    #lname = data['lname']
    #hquali = data['hquali']
    #email = data['email']
    #contact = data['contact']
    
    
    #print("separate",fname,lname,contact,email,username,password)
    #print(contact)
    try:
      connection = mysql.connector.connect(host='localhost',database='new1',user='root',password='am')
      query1 = """SET FOREIGN_KEY_CHECKS=0"""
      query = """DELETE from lecturer where lecturer_id = %s"""
      val = (lid,)
      query2 = """SET FOREIGN_KEY_CHECKS=1"""
      cursor = connection.cursor()
      res = cursor.execute(query1)
      result = cursor.execute(query, val)
      re = cursor.execute(query2)
      connection.commit()
      print("Record Deleted Successfully")

    except mysql.connector.Error as error :
      connection.rollback() #rollback if any exception occured
      print("Failed deleting record  {}".format(error))

    finally:
      #closing database connection.
      if(connection.is_connected()):
          cursor.close()
          connection.close()
          print("MySQL connection is closed")
    return 'OK'



@app.route("/student_submit",methods=['POST'])
def submit_student():
    data = json.loads(request.data)
    rno = data['rno']
    fname = data['fname']
    lname = data['lname']
    email = data['email']
    contact = data['contact']
    gender = data['gender']
    faculty = data['faculty']

    
    
    #print("separate",fname,lname,contact,email,username,password)
    print(gender)
    print(faculty)
    try:
      connection = mysql.connector.connect(host='localhost',database='new1',user='root',password='am')
      query1 = """SET FOREIGN_KEY_CHECKS=0"""
      query = """INSERT INTO student (roll_no, first_name, last_name, email_id,contact_no, gender, faculty) VALUES (%s, %s, %s, %s, %s, %s, %s) """
      val = (rno, fname, lname, email, contact, gender, faculty)
      query2 = """SET FOREIGN_KEY_CHECKS=1"""
      cursor = connection.cursor()
      res = cursor.execute(query1)
      result = cursor.execute(query, val)
      re = cursor.execute(query2)
      connection.commit()
      print("Record Inserted Successfully")

    except mysql.connector.Error as error :
      connection.rollback() #rollback if any exception occured
      print("Failed inserting record  {}".format(error))

    finally:
      #closing database connection.
      if(connection.is_connected()):
          #cursor.close()
          #connection.close()
          print("MySQL connection is closed")
    return 'OK'



@app.route("/student_update",methods=['POST'])
def update_student():
    data = json.loads(request.data)
    rno = data['rno']
    fname = data['fname']
    lname = data['lname']
    email = data['email']
    contact = data['contact']
    gender = data['gender']
    faculty = data['faculty']
    
    
    #print("separate",fname,lname,contact,email,username,password)
    #print(contact)
    try:
      connection = mysql.connector.connect(host='localhost',database='new1',user='root',password='am')
      query1 = """SET FOREIGN_KEY_CHECKS=0"""
      query = """UPDATE student set first_name = %s, last_name = %s,  email_id = %s,contact_no = %s, gender = %s, faculty = %s where roll_no = %s """
      val = ( fname, lname, email, contact, gender, faculty, rno)
      query2 = """SET FOREIGN_KEY_CHECKS=1"""
      cursor = connection.cursor()
      res = cursor.execute(query1)
      result = cursor.execute(query, val)
      re = cursor.execute(query2)
      connection.commit()
      print("Record Updated Successfully")

    except mysql.connector.Error as error :
      connection.rollback() #rollback if any exception occured
      print("Failed updateing record  {}".format(error))

    finally:
      #closing database connection.
      if(connection.is_connected()):
          cursor.close()
          connection.close()
          print("MySQL connection is closed")
    return 'OK'

@app.route("/student_delete",methods=['POST'])
def delete_student():
    data = json.loads(request.data)
    rno = data['rno']
    
    try:
      connection = mysql.connector.connect(host='localhost',database='new1',user='root',password='am')
      query1 = """SET FOREIGN_KEY_CHECKS=0"""
      query = """DELETE from student where roll_no = %s"""
      val = (rno,)
      query2 = """SET FOREIGN_KEY_CHECKS=1"""
      cursor = connection.cursor()
      res = cursor.execute(query1)
      result = cursor.execute(query, val)
      re = cursor.execute(query2)
      connection.commit()
      print("Record Deleted Successfully")

    except mysql.connector.Error as error :
      connection.rollback() #rollback if any exception occured
      print("Failed deleting record  {}".format(error))

    finally:
      #closing database connection.
      if(connection.is_connected()):
          cursor.close()
          connection.close()
          print("MySQL connection is closed")
    return 'OK'


@app.route("/student-course_submit",methods=['POST'])
def submit_student_course():
    data = json.loads(request.data)
    rno = data['rno']
    course = data['course']
    lecturer = data['lecturer']
    

    
    
    #print("separate",fname,lname,contact,email,username,password)
    print(rno,course,lecturer)
    
    try:
      connection = mysql.connector.connect(host='localhost',database='new1',user='root',password='am')
      query1 = """SET FOREIGN_KEY_CHECKS=0"""
      query = """INSERT INTO stud_course (roll_no, course, lecturer) VALUES (%s, %s, %s) """
      val = (rno, course, lecturer)
      query2 = """SET FOREIGN_KEY_CHECKS=1"""
      cursor = connection.cursor()
      res = cursor.execute(query1)
      result = cursor.execute(query, val)
      re = cursor.execute(query2)
      connection.commit()
      print("Record Inserted Successfully")

    except mysql.connector.Error as error :
      connection.rollback() #rollback if any exception occured
      print("Failed inserting record  {}".format(error))

    finally:
      #closing database connection.
      if(connection.is_connected()):
          #cursor.close()
          #connection.close()
          print("MySQL connection is closed")
    return 'OK'


@app.route("/student-course_update",methods=['POST'])
def update_student_course():
    data = json.loads(request.data)
    rno = data['rno']
    course = data['course']
    lecturer = data['lecturer']
    
    
    #print("separate",fname,lname,contact,email,username,password)
    #print(contact)
    try:
      connection = mysql.connector.connect(host='localhost',database='new1',user='root',password='am')
      query1 = """SET FOREIGN_KEY_CHECKS=0"""
      query = """UPDATE stud_course set course = %s, lecturer = %s where roll_no = %s and course= %s and lecturer= %s """
      val = ( course, lecturer, rno)
      query2 = """SET FOREIGN_KEY_CHECKS=1"""
      cursor = connection.cursor()
      res = cursor.execute(query1)
      result = cursor.execute(query, val)
      re = cursor.execute(query2)
      connection.commit()
      print("Record Updated Successfully")

    except mysql.connector.Error as error :
      connection.rollback() #rollback if any exception occured
      print("Failed updateing record  {}".format(error))

    finally:
      #closing database connection.
      if(connection.is_connected()):
          cursor.close()
          connection.close()
          print("MySQL connection is closed")
    return 'OK'


@app.route("/student-course_delete",methods=['POST'])
def delete_student_course():
    data = json.loads(request.data)
    rno = data['rno']
    
    try:
      connection = mysql.connector.connect(host='localhost',database='new1',user='root',password='am')
      query1 = """SET FOREIGN_KEY_CHECKS=0"""
      query = """DELETE from stud_course where roll_no = %s"""
      val = (rno,)
      query2 = """SET FOREIGN_KEY_CHECKS=1"""
      cursor = connection.cursor()
      res = cursor.execute(query1)
      result = cursor.execute(query, val)
      re = cursor.execute(query2)
      connection.commit()
      print("Record Deleted Successfully")

    except mysql.connector.Error as error :
      connection.rollback() #rollback if any exception occured
      print("Failed deleting record  {}".format(error))

    finally:
      #closing database connection.
      if(connection.is_connected()):
          cursor.close()
          connection.close()
          print("MySQL connection is closed")
    return 'OK'


if __name__ == "__main__":
	app.run(host='127.0.0.1 ', port = 5000)